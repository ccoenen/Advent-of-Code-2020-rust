use std::io::{self, Read};
use std::fs::File;

#[derive(Copy, Clone, PartialEq, Debug)]
enum Content {
	OPEN,
	TREE,
	UNKNOWN,
}


struct Map {
	raw_map: Vec<Content>,
	width: u32,
	height: u32,
}


impl Map {
	pub fn new(input: &String) -> Map {
		let lines = input.lines();
		let height = lines.count();
		let mut width = 0;

		let mut map:Vec<Content> = vec![];

		for line in input.lines() {
			if width == 0 {
				width = line.chars().count();
			}
			let mut submap = line.chars().map(|c|
				if c =='.' {Content::OPEN}
				else if c == '#' {Content::TREE}
				else {Content::UNKNOWN}
			).collect::<Vec<Content>>();
			map.append(&mut submap);
		}
		Map {
			raw_map: map,
			width: width as u32,
			height: height as u32,
		}
	}

	fn get(&self, x: u32, y: u32) -> Content {
		return self.raw_map[((x % self.width) + self.width * y) as usize];
	}

	fn count_along_slope(&self, x_slope: u32, y_slope: u32, item: Content) -> u32 {
		let mut encountered_trees = 0;
		let mut x = 0;
		let mut y = 0;
		while y < self.height {
			if self.get(x, y) == item {
				encountered_trees += 1;
			}
			x += x_slope;
			y += y_slope;
		}
		return encountered_trees;
	}
}


fn main() -> io::Result<()> {
	let mut f = File::open("input-03.txt")?;
	let mut content = String::new();
	f.read_to_string(&mut content);
	let map = Map::new(&content);
	let trees = map.count_along_slope(3, 1, Content::TREE);

	println!("Found {} trees", trees);

	let mut multiplied_across_runs = map.count_along_slope(1, 1, Content::TREE);
	multiplied_across_runs *= map.count_along_slope(3, 1, Content::TREE);
	multiplied_across_runs *= map.count_along_slope(5, 1, Content::TREE);
	multiplied_across_runs *= map.count_along_slope(7, 1, Content::TREE);
	multiplied_across_runs *= map.count_along_slope(1, 2, Content::TREE);

	println!("Product is {}", multiplied_across_runs);

	Ok(())
}


#[cfg(test)]
mod tests {
	#[test]
	fn parses_nicely() {
		// these are taken directly from the examples on the site.
		let map_string = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
".to_string();
		let map = super::Map::new(&map_string);

		assert_eq!(map.get(0,0), super::Content::OPEN);
		assert_eq!(map.get(2,0), super::Content::TREE);
		assert_eq!(map.get(3,0), super::Content::TREE);
		assert_eq!(map.get(10,0), super::Content::OPEN);
	}

	#[test]
	fn repeats_x_direction() {
		let map_string = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
".to_string();
		let map = super::Map::new(&map_string);

		assert_eq!(map.get(0,0), super::Content::OPEN);
		assert_eq!(map.get(11,0), super::Content::OPEN);
		assert_eq!(map.get(22,0), super::Content::OPEN);
		assert_eq!(map.get(33,0), super::Content::OPEN);
		assert_eq!(map.get(0,1), super::Content::TREE);
		assert_eq!(map.get(11,1), super::Content::TREE);
		assert_eq!(map.get(22,1), super::Content::TREE);
		assert_eq!(map.get(33,1), super::Content::TREE);
	}

	#[test]
	fn counts_trees() {
		let map_string = "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
".to_string();
		let map = super::Map::new(&map_string);

		let mut trees = map.count_along_slope(1, 1, super::Content::TREE);
		assert_eq!(trees, 2);

		trees = map.count_along_slope(3, 1, super::Content::TREE);
		assert_eq!(trees, 7);

		trees = map.count_along_slope(5, 1, super::Content::TREE);
		assert_eq!(trees, 3);

		trees = map.count_along_slope(7, 1, super::Content::TREE);
		assert_eq!(trees, 4);

		trees = map.count_along_slope(1, 2, super::Content::TREE);
		assert_eq!(trees, 2);
	}
}
