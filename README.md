# Advent of Code 2020

My attempts at solving the 2020 Advent of Code riddles in rust.
https://adventofcode.com/2020/

I am using this to learn rust. I'm taking shortcuts that I can get away with because the inputs are know to me. Please don't take this a "how rust is supposed to look like". Especially the amount of error checking or the number of `.unwrap()` calls are probably not healthy rust code.

## How to start

Have Rust installed, this is described on https://rust-lang.org

Go into an individual directory and run `cargo run`, which should give you the output for my (included) puzzle input.

Also try `cargo test` to run the automated test that are included in each file.

## License

All code is GPLv3.
