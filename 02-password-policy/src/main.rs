use std::io::{self, BufReader};
use std::io::prelude::*;
use std::fs::File;

use lazy_static::lazy_static;
use regex::Regex;

lazy_static! {
	static ref LINE_REGEX:Regex = Regex::new(r"(?x)
		^ # anchor in front
		(?P<lower>\d+)-(?P<upper>\d+) # lower and upper bound
		\s+
		(?P<character>\w) # password character
		:\s+
		(?P<password>.+) # password
		$ # anchor in the back
	").unwrap();
}

struct Entry {
	lower_bound: u32,
	upper_bound: u32,
	character: char,
	password: String,
}

impl Entry {
	pub fn new(line: &String) -> Entry {
		let cap = LINE_REGEX.captures(line).unwrap();

		Entry {
			lower_bound: cap.name("lower").unwrap().as_str().parse().unwrap(),
			upper_bound: cap.name("upper").unwrap().as_str().parse().unwrap(),
			character: cap.name("character").unwrap().as_str().chars().nth(0).unwrap(),
			password: cap.name("password").unwrap().as_str().to_string(),
		}
	}

	fn is_valid_in_current_job(&self) -> bool {
		let chars:Vec<char> = self.password.chars().collect();
		let first_char = chars[(self.lower_bound - 1) as usize];
		let second_char = chars[(self.upper_bound - 1) as usize];

		let found_first = first_char == self.character;
		let found_second = second_char == self.character;

		return found_first ^ found_second; // only _exactly_ one.
	}

	fn is_valid_in_other_job(&self) -> bool {
		let mut counter = 0;
		for char in self.password.chars() {
			if char == self.character {
				counter += 1;
			}
		}
		return self.lower_bound <= counter && counter <= self.upper_bound;
	}
}


fn main() -> io::Result<()> {
	let f = File::open("input-02.txt")?;
	let f = BufReader::new(f);

	let mut other_job_valid_passwords:u32 = 0;
	let mut other_job_invalid_passwords:u32 = 0;
	let mut current_job_valid_passwords:u32 = 0;
	let mut current_job_invalid_passwords:u32 = 0;

	for raw_line in f.lines() {
		let line = raw_line.unwrap();
		let entry = Entry::new(&line);
		if entry.is_valid_in_other_job() {
			other_job_valid_passwords+=1;
			// println!("+ valid: {}", &line);
		} else {
			other_job_invalid_passwords+=1;
			// println!("- INVALID: {}", &line);
		}

		if entry.is_valid_in_current_job() {
			current_job_valid_passwords+=1;
			// println!("+ valid: {}", &line);
		} else {
			current_job_invalid_passwords+=1;
			// println!("- INVALID: {}", &line);
		}
	}
	println!("OTHER job: found {} valid and {} invalid passwords", other_job_valid_passwords, other_job_invalid_passwords);
	println!("CURRENT job: found {} valid and {} invalid passwords", current_job_valid_passwords, current_job_invalid_passwords);

	Ok(())
}


#[cfg(test)]
mod tests {
	#[test]
	fn parses_nicely() {
		// these are taken directly from the examples on the site.
		let entry = super::Entry::new(&"1-3 a: abcde".to_string());
		assert_eq!(entry.lower_bound, 1);
		assert_eq!(entry.upper_bound, 3);
		assert_eq!(entry.character, 'a');
		assert_eq!(entry.password, "abcde");
	}

	#[test]
	fn decides_current_job_validity_correctly() {
		let entries: Vec<super::Entry> = vec![
			// these are taken directly from the examples on the site.
			super::Entry::new(&"1-3 a: abcde".to_string()),
		];

		for entry in entries {
			assert!(entry.is_valid_in_current_job());
		}
	}

	#[test]
	fn decides_current_job_invalidity_correctly() {
		let entries: Vec<super::Entry> = vec![
			// these are taken directly from the examples on the site.
			super::Entry::new(&"1-3 b: cdefg".to_string()),
			super::Entry::new(&"2-9 c: ccccccccc".to_string()),
		];

		for entry in entries {
			assert!(!entry.is_valid_in_current_job());
		}
	}

	#[test]
	fn decides_other_job_validity_correctly() {
		let entries: Vec<super::Entry> = vec![
			// these are taken directly from the examples on the site.
			super::Entry::new(&"1-3 a: abcde".to_string()),
			super::Entry::new(&"2-9 c: ccccccccc".to_string()),
			// this is from myself
			super::Entry::new(&"2-9 c: cXcXcXcXcXcXcXcXc".to_string()),
		];

		for entry in entries {
			assert!(entry.is_valid_in_other_job());
		}
	}

	#[test]
	fn decides_other_job_invalidity_correctly() {
		let entries: Vec<super::Entry> = vec![
			// these are taken directly from the examples on the site.
			super::Entry::new(&"1-3 b: cdefg".to_string()),
		];

		for entry in entries {
			assert!(!entry.is_valid_in_other_job());
		}
	}

	#[test]
	fn valid_regex() {
		// these are taken directly from the examples on the site.
		assert!(super::LINE_REGEX.is_match("1-3 a: abcde"));
		assert!(super::LINE_REGEX.is_match("1-3 b: cdefg"));
		assert!(super::LINE_REGEX.is_match("2-9 c: ccccccccc"));

		assert!(!super::LINE_REGEX.is_match("29 c: ccccccccc"));
		assert!(!super::LINE_REGEX.is_match("2-9 c ccccccccc"));
		assert!(!super::LINE_REGEX.is_match("-9 c: ccccccccc"));
	}

	#[test]
	fn regex_capture_groups() {
		// these are taken directly from the examples on the site.
		let cap = super::LINE_REGEX.captures("2-9 c: ccccccccc").unwrap();
		assert_eq!(cap.name("lower").unwrap().as_str(), "2");
		assert_eq!(cap.name("upper").unwrap().as_str(), "9");
		assert_eq!(cap.name("character").unwrap().as_str(), "c");
		assert_eq!(cap.name("password").unwrap().as_str(), "ccccccccc");
	}
}
