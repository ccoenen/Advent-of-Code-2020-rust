use std::io::{self, BufReader};
use std::io::prelude::*;
use std::fs::File;

fn main() -> io::Result<()> {
	let f = File::open("input-01.txt")?;
	let f = BufReader::new(f);

	let mut expenses:Vec<i32> = vec![];

	for line in f.lines() {
		let text: String = line.unwrap();
		let expense: i32 = text.parse().unwrap();
		expenses.push(expense)
	}

	let pair = find_pair(&expenses, 2020);
	println!("found {} and {}", pair[0], pair[1]);
	println!("product: {}", pair[0] * pair[1]);

	let pair = find_trio(&expenses, 2020);
	println!("found {}, {} and {}", pair[0], pair[1], pair[2]);
	println!("product: {}", pair[0] * pair[1] * pair[2]);

	Ok(())
}

// find a pair of numbers in a list that add up to a requested sum
fn find_pair(input_list: &Vec<i32>, goal: i32) -> Vec<i32> {
	let list:Vec<i32> = input_list.to_vec();
	for first_number in &list {
		for second_number in &list {
			if first_number + second_number == goal {
				return vec![*first_number, *second_number]
			}
		}
	}
	return vec![]
}

// find three numbers in a list that add up to a requested sum
fn find_trio(input_list: &Vec<i32>, goal: i32) -> Vec<i32> {
	let list:Vec<i32> = input_list.to_vec();
	for first_number in &list {
		for second_number in &list {
			for third_number in &list {
				if first_number + second_number + third_number == goal {
					return vec![*first_number, *second_number, *third_number]
				}
			}
		}
	}
	return vec![]
}


#[cfg(test)]
mod tests {
	#[test]
	fn finds_correct_pairs() {
		assert_eq!(super::find_pair(&vec![12, 15, 10], 22), vec![12, 10]);
		// these are taken directly from the examples on the site.
		assert_eq!(super::find_pair(&vec![1721, 979, 366, 299, 675, 1456], 2020), vec![1721, 299]);
	}

	#[test]
	fn finds_correct_trios() {
		assert_eq!(super::find_trio(&vec![12, 15, 10, 100], 122), vec![12, 10, 100]);
		// these are taken directly from the examples on the site.
		assert_eq!(super::find_trio(&vec![1721, 979, 366, 299, 675, 1456], 2020), vec![979, 366, 675]);
	}
}
